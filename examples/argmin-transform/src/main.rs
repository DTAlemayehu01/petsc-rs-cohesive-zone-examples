// Copyright 2018-2024 argmin developers
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

static HELP_MSG: &str = "Rosenbrock Argmin Test Problem Imported to Petsc-rs.\n\n";

use argmin::core::{Error, Gradient, Hessian};
use argmin_testfunctions::{rosenbrock_derivative, rosenbrock_hessian};
use ndarray::{Array, Array1, Array2};
use petsc::{mpi::traits::*, prelude::*, Petsc, Scalar};

struct Rosenbrock {}

impl Gradient for Rosenbrock {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, p: &Self::Param) -> Result<Self::Gradient, Error> {
        Ok(Array1::from(rosenbrock_derivative(&p.to_vec())))
    }
}

impl Hessian for Rosenbrock {
    type Param = Array1<f64>;
    type Hessian = Array2<f64>;

    fn hessian(&self, p: &Self::Param) -> Result<Self::Hessian, Error> {
        let h = rosenbrock_hessian(&p.to_vec())
            .into_iter()
            .flatten()
            .collect();
        Ok(Array::from_shape_vec((p.len(), p.len()), h)?)
    }
}

struct Opt {}

impl petsc::Opt for Opt {
    fn from_opt_builder(_pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        Ok(Opt {})
    }
}

fn example_solve(petsc: &Petsc, opt: Opt, mut cz: Rosenbrock) -> petsc::Result<()> {
    #[allow(non_snake_case)]
    #[allow(unused_variables)]
    let Opt = opt;
    let n = 2;

    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            petsc::ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    // Vector for solution + nonlinear function
    let mut x = petsc.vec_create()?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut r = x.duplicate()?;
    x.set_name("soln")?;

    // Jacobian matrix DS
    let mut jacobian = petsc.mat_create()?;
    jacobian.set_sizes(None, None, n, n)?;
    jacobian.set_from_options()?;
    jacobian.set_up()?;

    //solver context
    let mut snes = petsc.snes_create()?;

    let struct_eq = &mut cz;

    snes.set_argmin_function(&mut r, struct_eq)?;
    snes.set_argmin_jacobian(&mut jacobian, struct_eq)?;

    let mut ksp = snes.ksp_or_create()?;
    let pc = ksp.pc_or_create()?;
    pc.set_type(petsc::PCType::PCNONE)?;
    ksp.set_tolerances(Some(1.0e-4), None, None, Some(20))?;

    snes.set_from_options()?;

    x.set_all(Scalar::from(1.2))?;

    snes.solve(None, &mut x)?;

    let viewer = petsc::Viewer::create_ascii_stdout(petsc.world())?;
    x.view_with(Some(&viewer))?;

    Ok(())
}

fn main() -> petsc::Result<()> {
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    let opt = petsc.options()?;
    let Opt {} = &opt;

    let model = Rosenbrock {};

    example_solve(&petsc, opt, model)
}
