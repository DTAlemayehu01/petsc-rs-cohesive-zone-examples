//! Concepts: SNES optimization example
//!
//!
//! To run:
//! ```text
//! $ cargo build --bin snes-cohesive-zone
//! $ mpiexec -n 1 target/debug/snes-cohesive-zone
//! ```

static HELP_MSG: &str = "Cohesive Zone Model Test Problem.\n\n";

use argmin::core::{CostFunction, Error, Gradient, Hessian};
use petsc::{
    mpi::traits::*, prelude::*, InsertMode, Mat, MatAssemblyType, Petsc, Real, Scalar, SNES,
};
use std::f64::consts::E;

struct Opt {
    verbose: bool,
    c1: Real,
    c2: Real,
    k: Real,
    dh: Real,
    init_guess: Real,
    algorithm: String,
    argmin: bool,
}

impl petsc::Opt for Opt {
    fn from_opt_builder(pob: &mut petsc::OptBuilder) -> petsc::Result<Self> {
        let verbose = pob.options_bool("-verbose", "", "snes-cohesive-zone", true)?;
        let c1 = pob.options_real("-c1", "", "snes-cohesive-zone", 3.0)?;
        let c2 = pob.options_real("-c2", "", "snes-cohesive-zone", 2.0)?;
        let k = pob.options_real("-k", "", "snes-cohesive-zone", 1.0)?;
        let dh = pob.options_real("-dh", "", "snes-cohesive-zone", 4.0)?;
        let init_guess = pob.options_real("-guess", "", "snes-cohesive-zone", 2.8)?;
        let algorithm =
            pob.options_from_string("-algo", "", "snes-cohesive-zone", "newton".to_string())?;
        let argmin = pob.options_bool("-argmin", "", "snes-cohesive-zone", false)?;
        Ok(Opt {
            verbose,
            c1,
            c2,
            k,
            dh,
            init_guess,
            algorithm,
            argmin,
        })
    }
}

// Begin argmin Model
struct CohesiveZone {
    sigma_c: f64,
    delta_c: f64,
    k: f64,
    dh: f64,
}

impl CohesiveZone {
    fn cohesive_energy(&self, x: f64) -> f64 {
        let d = self.delta_c;
        -self.sigma_c * (d + x) * (1.0 - x / d).exp()
    }
    fn cohesive_traction(&self, x: f64) -> f64 {
        let d = self.delta_c;
        (self.sigma_c / d) * x * (1.0 - x / d).exp()
    }
    fn d_cohesive_traction(&self, x: f64) -> f64 {
        (self.sigma_c / self.delta_c) * (1.0 - x / self.delta_c) * (1.0 - x / self.delta_c).exp()
    }
    fn elastic_energy(&self, x: f64) -> f64 {
        0.5 * self.k * (x - self.dh).powi(2)
    }
    fn elastic_traction(&self, x: f64) -> f64 {
        self.k * (x - self.dh)
    }
    fn d_elastic_traction(&self, _x: f64) -> f64 {
        self.k
    }
}
//End argmin Model

impl CostFunction for CohesiveZone {
    type Param = f64;
    type Output = f64;

    fn cost(&self, x: &Self::Param) -> Result<Self::Output, Error> {
        let x = *x;
        Ok(self.cohesive_energy(x) + self.elastic_energy(x))
    }
}

impl Gradient for CohesiveZone {
    type Param = f64;
    type Gradient = f64;

    fn gradient(&self, x: &Self::Param) -> Result<Self::Gradient, Error> {
        let x = *x;
        Ok(self.cohesive_traction(x) + self.elastic_traction(x))
    }
}

impl Hessian for CohesiveZone {
    type Param = f64;
    type Hessian = f64;

    fn hessian(&self, x: &Self::Param) -> Result<Self::Hessian, Error> {
        let x = *x;
        Ok(self.d_cohesive_traction(x) + self.d_elastic_traction(x))
    }
}

fn main() -> petsc::Result<()> {
    let petsc = Petsc::builder()
        .args(std::env::args())
        .help_msg(HELP_MSG)
        .init()?;

    let opt = petsc.options()?;
    let Opt {
        verbose: _,
        c1: _,
        c2: _,
        k: _,
        dh: _,
        init_guess: _,
        algorithm,
        argmin: _,
    } = &opt;

    let model = CohesiveZone {
        sigma_c: opt.c1,
        delta_c: opt.c2,
        k: opt.k,
        dh: opt.dh,
    };

    if algorithm == "newton" {
        example_solve(&petsc, opt, model)
    } else if algorithm == "newton_cg" {
        example_solve_cg(&petsc, opt, model)
    } else if opt.argmin {
        argmin_solve(&petsc, opt, model)
    } else {
        Ok(())
    }
}

fn argmin_solve(petsc: &Petsc, opt: Opt, cz: CohesiveZone) -> petsc::Result<()> {
    let Opt {
        verbose,
        c1: _,
        c2: _,
        k: _,
        dh: _,
        init_guess,
        algorithm: _,
        argmin: _,
    } = opt;
    let n = 1;

    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            petsc::ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    // Vector for solution + nonlinear function
    let mut x = petsc.vec_create()?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut r = x.duplicate()?;
    x.set_name("soln")?;

    // Jacobian matrix DS
    let mut jacobian = petsc.mat_create()?;
    jacobian.set_sizes(None, None, n, n)?;
    jacobian.set_from_options()?;
    jacobian.set_up()?;

    //solver context
    let mut snes = petsc.snes_create()?;

    //setting functions via closures
    snes.set_argmin_objective_f64(&cz)?;
    snes.set_argmin_function_f64(&mut r, &cz)?;

    let mut ksp = snes.ksp_or_create()?;
    let pc = ksp.pc_or_create()?;
    pc.set_type(petsc::PCType::PCNONE)?;
    ksp.set_tolerances(Some(1.0e-4), None, None, Some(20))?;

    snes.set_from_options()?;

    x.set_all(Scalar::from(init_guess))?;

    snes.solve(None, &mut x)?;
    let viewer = petsc::Viewer::create_ascii_stdout(petsc.world())?;
    if verbose {
        x.view_with(Some(&viewer))?;
    }

    Ok(())
}

fn example_solve(petsc: &Petsc, opt: Opt, _cz: CohesiveZone) -> petsc::Result<()> {
    let Opt {
        verbose,
        c1,
        c2,
        k,
        dh,
        init_guess,
        algorithm: _,
        argmin: _,
    } = opt;
    let n = 1;

    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            petsc::ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    // Vector for solution + nonlinear function
    let mut x = petsc.vec_create()?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut r = x.duplicate()?;
    x.set_name("soln")?;

    // Jacobian matrix DS
    let mut jacobian = petsc.mat_create()?;
    jacobian.set_sizes(None, None, n, n)?;
    jacobian.set_from_options()?;
    jacobian.set_up()?;

    //solver context
    let mut snes = petsc.snes_create()?;

    //setting functions via closures
    snes.set_function(
        &mut r,
        |_snes: &SNES, x: &petsc::Vec, f: &mut petsc::Vec| {
            let x_view = x.view()?;
            let mut f_view = f.view_mut()?;

            f_view[0] =
                (c1 / c2) * x_view[0] * E.powf(1.0 - (x_view[0] / c2)) + k * x_view[0] - k * dh;

            Ok(())
        },
    )?;
    snes.set_jacobian_single_mat(
        &mut jacobian,
        |_snes: &SNES, x: &petsc::Vec, jac: &mut Mat| {
            let x_view = x.view()?;

            jac.assemble_with(
                [(
                    0,
                    0,
                    (c1 / c2) * E.powf(1.0 - (x_view[0] / c2))
                        - ((c1 * x_view[0]) / (c2.powf(2.0)) * E.powf(1.0 - (x_view[0] / c2)))
                        + k,
                )],
                InsertMode::INSERT_VALUES,
                MatAssemblyType::MAT_FINAL_ASSEMBLY,
            )?;

            Ok(())
        },
    )?;

    let mut ksp = snes.ksp_or_create()?;
    let pc = ksp.pc_or_create()?;
    pc.set_type(petsc::PCType::PCNONE)?;
    ksp.set_tolerances(Some(1.0e-4), None, None, Some(20))?;

    snes.set_from_options()?;

    x.set_all(Scalar::from(init_guess))?;

    snes.solve(None, &mut x)?;
    let viewer = petsc::Viewer::create_ascii_stdout(petsc.world())?;
    if verbose {
        x.view_with(Some(&viewer))?;
    }

    Ok(())
}

fn example_solve_cg(petsc: &Petsc, opt: Opt, _cz: CohesiveZone) -> petsc::Result<()> {
    let Opt {
        verbose,
        c1,
        c2,
        k,
        dh,
        init_guess,
        algorithm: _,
        argmin: _,
    } = opt;
    let n = 1;

    if petsc.world().size() != 1 {
        Petsc::set_error(
            petsc.world(),
            petsc::ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
            "This is a uniprocessor example only!",
        )?;
    }

    let mut x = petsc.vec_create()?;
    x.set_sizes(None, n)?;
    x.set_from_options()?;
    let mut r = x.duplicate()?;
    x.set_name("soln")?;

    let mut jacobian = petsc.mat_create()?;
    jacobian.set_sizes(None, None, n, n)?;
    jacobian.set_from_options()?;
    jacobian.set_up()?;

    let mut snes = petsc.snes_create()?;

    snes.set_objective(|_snes: &SNES, x: &petsc::Vec| {
        let x_view = x.view()?;

        let d = c2;

        let cohesive_energy = -c1 * (d + x_view[0]) * (1.0 - x_view[0] / d).exp();
        let elastic_energy = 0.5 * k * (x_view[0] - dh).powi(2);
        let obj = cohesive_energy + elastic_energy;

        //let obj = -c1*E.powf(1.0 - (x_view[0]/c2) )*(c2 + x_view[0]) + (k/2.0)*x_view[0]*(-2.0*dh+x_view[0]);
        Ok(obj)
    })?;

    snes.set_function(
        &mut r,
        |_snes: &SNES, x: &petsc::Vec, f: &mut petsc::Vec| {
            let x_view = x.view()?;
            let mut f_view = f.view_mut()?;

            let d = c2;
            let cohesive_traction = (c1 / d) * x_view[0] * (1.0 - x_view[0] / d).exp();
            let elastic_traction = k * (x_view[0] - dh);

            f_view[0] = cohesive_traction + elastic_traction;

            Ok(())
        },
    )?;

    let mut ksp = snes.ksp_or_create()?;
    let pc = ksp.pc_or_create()?;
    pc.set_type(petsc::PCType::PCNONE)?;
    ksp.set_tolerances(Some(1.0e-4), None, None, Some(20))?;

    snes.set_from_options()?;

    x.set_all(Scalar::from(init_guess))?;

    snes.solve(None, &mut x)?;

    let viewer = petsc::Viewer::create_ascii_stdout(petsc.world())?;
    if verbose {
        x.view_with(Some(&viewer))?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    //use rand::random;

    /*
    //reserved for agmin tests
    #[test]
    fn test_newton() {
        for _ in 1..1001 {
            let v = [random(), random(), random(), random(), random()];
            if let Err(ref e) = run_newton(&v) {
                println!("{e}");
            };
        }
    }

    //Not Implemented
    #[test]
    fn test_newton_cg() {
        for _ in 1..1001 {
            let v = [random(), random(), random(), random(), random()];
            if let Err(ref e) = run_newton_cg(&v) {
                println!("{e}");
            };
        }
    }
    */

    #[test]
    fn snes_example_run() {
        let petsc = Petsc::init_no_args().expect("failed to initalize PETSc");

        // Easy
        let opt = Opt {
            verbose: false,
            c1: 3.0,
            c2: 2.0,
            k: 1.0,
            dh: 4.0,
            init_guess: 2.8,
            algorithm: "newton".to_string(),
            argmin: false,
        };

        let model = CohesiveZone {
            sigma_c: opt.c1,
            delta_c: opt.c2,
            k: opt.k,
            dh: opt.dh,
        };

        assert!(example_solve(&petsc, opt, model).is_ok());

        // Hard
        let opt = Opt {
            verbose: false,
            c1: 3.0,
            c2: 2.0,
            k: 1.0,
            dh: 4.0,
            init_guess: 2.8,
            algorithm: "newton_cg".to_string(),
            argmin: false,
        };

        let model = CohesiveZone {
            sigma_c: opt.c1,
            delta_c: opt.c2,
            k: opt.k,
            dh: opt.dh,
        };

        assert!(example_solve_cg(&petsc, opt, model).is_ok());
    }
}
