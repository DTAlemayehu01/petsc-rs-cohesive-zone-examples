// Rust does not provide a way to set RPATH/RUNPATH for Linux, thus one needs to
// set LD_LIBRARY_PATH to find a shared libpetsc when it is not in a system
// path. This demonstrates a way to set this variable automatically in `cargo
// run` and `cargo test`. Note that this must be done in a `bin` crate, not in
// the petsc `lib` crate. Note that only Linux is supported below.

fn main() {
    #[cfg(target_os = "linux")]
    {
        let petsc_lib = build_probe_petsc::probe("3.21");
        println!(
            "cargo:rustc-env=LD_LIBRARY_PATH={}",
            petsc_lib.lib.link_paths[0].to_string_lossy()
        );
    }
}
